/**
 * Created by gladyu on 17/4/21.
 */
var game = new Vue({
    el: '.game',
    data: {
        "headers": [],
        "gamelists": [],
        "and_num":0,
        "ios_num":0
    },
    methods: {
        initData: function (name) {
            var self = this;
            var filepath = './' + name + '.json';
            $.ajax({
                url: filepath,
                async: false,
                dataType: 'json',
                success: function (data) {
                    if(data.status==0){
                        self.headers = data.headers;
                        self.gamelists = data.gamelists;
                        self.setLen(self.gamelists);
                    }
                }
            })
        },
        show:function(system){
            var showStatus = false;
            if(system == 'all'){
                showStatus = true;
            }else if(system == 'android' && is_android){
                showStatus = true;
            }else if(system == 'ios' && (is_ios || is_iPad)){
                showStatus = true;
            }
            return showStatus;
        },
        download:function(url,name){
            pageCnzz(name);
            var downurl = is_android ? url.android : url.ios;
            if(!!window.WKAPI && is_android){
                window.WKAPI.allowDownloadForOnce();
            }
            window.location.href = downurl;
        },
        showLine:function(index){
            var self = this;
            var len = is_android ? self.and_num : self.ios_num;
            if(index == len){
                return false;
            }
            return true;
        },
        setLen:function(data){
            var self = this;
            for(var i=0;i< data.length;i++){
                if(data[i].sys == 'all'){
                    self.and_num++;
                    self.ios_num++;
                }else if(data[i].sys == 'android'){
                    self.and_num++;
                }else if(data[i].sys == 'ios'){
                    self.ios_num++;
                }
            }
        },
        removeColor:function(ind){
            $($('.game-down')[ind-1]).addClass("no-select");
        }
    }
});
game.initData('games');
var pageCnzz = function(label) {
    if (typeof _czc == 'undefined') {
    } else {
        _czc.push(['_trackEvent', '游戏吧', '点击', label]);
    }
};