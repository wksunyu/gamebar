/**
 * Created by gladyu on 16/11/3.
 */
;var ua = navigator.userAgent.toLowerCase();
var is_ios = /iphone/.test(ua);
var is_iPad = /ipad/.test(ua);
var is_android = /android/i.test(ua);
var is_devices = is_android || is_ios || is_iPad;
$().ready = adapterRem();
function adapterRem() {
    //rem adapter
    var dpr, rem, scale;
    var docEl = document.documentElement;
    var fontEl = document.createElement('style');
    var metaEl = document.querySelector('meta[name="viewport"]');
    dpr = window.devicePixelRatio || 1;
    if (is_devices) {
        rem = parseFloat(docEl.clientWidth * dpr) / 7.5;
    } else {
        rem = 50;
    }
    scale = 1 / dpr;
    metaEl.setAttribute('content', 'width=' + dpr * docEl.clientWidth + ',initial-scale=' + scale +
        ',maximum-scale=' + scale + ',minimum-scale=' + scale + ',user-scalable=no');

    docEl.setAttribute('data-dpr', dpr);
    docEl.firstElementChild.appendChild(fontEl);
    fontEl.innerHTML = "html{font-size:" + rem + "px!important;}";
    console.log(rem);
};
